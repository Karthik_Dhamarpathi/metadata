public class Emails 
{
  public static final String OWE_YP_SFDC_NOREPLY = 'NO REPLY - YP Salesforce.com';
  public static final String OWE_SOBJECT_TYPE = sObjectType.OrgWideEmailAddress.getName();

  public static Messaging.Singleemailmessage GetTemplatedEmail(Id TemplateId, Id orgwideId, Id targetObjId, Id whatId, Boolean saveAsActivity)
  {    
    Messaging.Singleemailmessage singleE = new Messaging.Singleemailmessage();
    
    singleE.setWhatId(whatId);
    singleE.setTemplateId(TemplateId);  
    singleE.setTargetObjectId(targetObjId);
    singleE.setSaveAsActivity(saveAsActivity);    
    singleE.setOrgWideEmailAddressId(orgwideId);
    
    return singleE;
  }
}